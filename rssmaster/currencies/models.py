from djmoney.models.fields import CurrencyField, MoneyField
from django.db import models


class ExchangeRate(models.Model):
    rate = MoneyField(db_index=True, max_digits=14, decimal_places=4,
                      null=False, blank=False)
    main_currency = CurrencyField(db_index=True, null=False, blank=False)
    date_time = models.DateTimeField(db_index=True, null=False, blank=False)
    timestamp = models.DateTimeField(auto_now_add=True, null=False, blank=True)

    def __str__(self):
        return str(self.rate)

    class Meta:
        unique_together = (("rate_currency", "main_currency", "date_time"),)
