from bs4 import BeautifulSoup
from requests import get


def get_html(url):
    r = get(url)
    r.raise_for_status()
    return r.text


def get_soup(html, parser="html.parser"):
    soup = BeautifulSoup(html, parser)
    return soup


def get_rss_currency_urls(soup):
    all_rss = soup.findAll('a', {'class': 'rss'}, href=True)
    rss_urls = []
    for rss in all_rss:
        if rss['href'].startswith("/rss/fxref-"):
            rss_urls.append(rss['href'])
    return rss_urls
