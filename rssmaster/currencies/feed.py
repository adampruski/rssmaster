from feedparser import parse
from datetime import datetime
from pytz import UTC
from time import mktime


def get_entries(url):
    feed = parse(url)
    return feed["entries"]


def get_entry_data(entry):
    rate, main_currency = entry["cb_exchangerate"].split()
    naive_date = datetime.fromtimestamp(mktime(entry["updated_parsed"]))
    date = naive_date.replace(tzinfo=UTC)
    entry_data = {
        "date_time": date,
        "rate": float(rate),
        'currency': entry["cb_targetcurrency"],
        "main_currency": main_currency
    }
    return entry_data
