from .models import ExchangeRate
from rest_framework import serializers


class ExchangeRateSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = ExchangeRate
        fields = ('url', 'rate', 'rate_currency', 'main_currency',
                  'date_time', 'timestamp',)
