from django.conf import settings
from django.shortcuts import render
from django.views.generic import TemplateView
from djmoney.money import Money
from rest_framework import viewsets
from .feed import get_entries, get_entry_data
from .scraper import get_html, get_rss_currency_urls, get_soup
from .models import ExchangeRate
from .serializers import ExchangeRateSerializer


class HomePageView(TemplateView):
    template_name = "home.html"


def update_ecb_rates(request):
    html = get_html(settings.ECB_URL)
    soup = get_soup(html)
    rss_urls = get_rss_currency_urls(soup)
    new_rates = []
    for rss in rss_urls:
        for entry in get_entries(settings.ECB_MAIN_URL + rss):
            data = get_entry_data(entry)
            if not ExchangeRate.objects.filter(
                    rate_currency=data["currency"],
                    date_time=data["date_time"],
                    main_currency=data["main_currency"]).exists():
                rate = Money(data["rate"], data["currency"])
                new_rates.append(ExchangeRate(
                    rate=rate,
                    main_currency=data["main_currency"],
                    date_time=data["date_time"]
                ))
    ExchangeRate.objects.bulk_create(new_rates)
    return render(request, "updated.html")


class ExchangeRateViewSet(viewsets.ModelViewSet):
    queryset = ExchangeRate.objects.all().order_by('-timestamp')
    serializer_class = ExchangeRateSerializer
