from django.contrib import admin
from django.urls import path, include
from rest_framework import routers
from currencies.views import HomePageView, update_ecb_rates, ExchangeRateViewSet

router = routers.DefaultRouter()
router.register(r'exchange_rate', ExchangeRateViewSet)


urlpatterns = [
    path('', HomePageView.as_view(), name="home"),
    path('update', update_ecb_rates, name="update"),
    path('', include(router.urls)),
    path('admin/', admin.site.urls),
]
