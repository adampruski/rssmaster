# RSSMaster
App for scraping European Central Bank rss urls and exchange rates.

## How it works
Scraper go to https://www.ecb.europa.eu/home/html/rss.en.html where it's searching for currencies rss links and scrap it all (requests, beautifulsoup). Later app download entries  (feedparser) which are filtered for duplicates in database and finally bulk create put data to do database. For now API allows only to get data.

## Installation
Go to project directory and downloads required packages:
```
pip install -r requirements.txt
```

## Usage
Simply run this command in rssmaster path:
```
./manage.py makemigrations
./manage.py migrate
./manage.py runserver
```
URL: http://localhost:8000
API: http://localhost:8000/exchange_rate

## Todo
- Documentation for functions,
- Improve scraper speed and create exceptions when connection or server is unavailable,
- More API options for all CRUD functions,
- Tests for scraper.py, feed.py, views.py and models.py,
- Celery for scheduled rates update (for ex. at 2:30pm)
- Dockerfile and docker-compose for production use.
